const mongoose =require('mongoose')

const profileSchema = new mongoose.Schema({
    ID:{
        type:Number
    },
    firstName:{
        type:String,
        required: [true, "First name is required."]
    },
    lastName:{
        type: String,
        required: [true, 'Last name is required.']
    },
    birthdate:{
        type: String,
        required: [true, 'Birthdate is required.']
    },
    gender:{
        type: String,
        required: [true, 'gender is required.']
    },
    maritalStatus:{
        type:String,
        required: [true, 'Marital Status is required.']
    },
    department:{
        type:String,
        required: [true, 'Department is required.']
    },
    position:{
        type:String,
        required: [true, 'Position is required.']
    },
    dateHired:{
        type:String,
        required: [true, 'Date Hired is required.']
    },
    employmentStatus:{
        type: String,
        required: [true, 'Employement status is required.']
    },
    contactNumber:{
        type:Number,
        required: [true, 'Employement status is required.']
    },
    email:{
        type:String
    },
    address:{
        type: String
    },
    city:{
        type: String
    },
    province:{
        type: String
    },
    nationality:{
        type:String
    },
    createdAt:{
        type: Date,
        default: new Date()
    },
    updatedAt:{
        type: String,
        default: new Date()
    }
})

module.exports= mongoose.model('profile', profileSchema);