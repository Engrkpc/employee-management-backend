const express = require('express');
const router = express.Router();
const ProfileController =  require('../controllers/profile');

// for image uploads
// const multer =  require('multer');
// const upload =  multer({dest:'uploads/'});


//adding new employee
router.post('/',(req, res) => {
    // will call the addProfile controller and pass req.body then execute.
    ProfileController.addProfile(req.body)
    //this will return true to console
    .then(data =>{
        console.log(req.body)
        res.send(data)
    }); 
})

router.get('/', (req,res) =>{
    ProfileController.get()
    // send the data to be used in front end
    .then(data =>{
        console.log(data)
        res.send(data)
    })
})

router.get('/viewDetails/:id', async (req, res) =>{
    const id = req.params.id
    console.log(req.params.id)
    //pass the id  {id} so that it can be destructure at the controller
    await ProfileController.viewDetails({id})
     // send the data to be used in front end
    .then( data =>{
        console.log(data)
        res.send(data)
    })
})

router.delete('/:id', (req,res) =>{
    const id =  req.params.id
    console.log(id)
    ProfileController.delete({id}).then(data => res.send(data) )
})

router.put('/editProfile/:id', (req,res) =>{
    const updatedData = req.body
    const id = req.params.id
    ProfileController.edit({id, updatedData}).then(data => res.send(data))
})


module.exports = router