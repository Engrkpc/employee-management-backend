
const Profile = require('../models/profile');


module.exports.addProfile = (params) =>{
    let profile = new Profile({
        firstName: params.firstName,
        lastName: params.lastName,
        birthdate: params.birthdate,
        gender: params.gender,
        maritalStatus:params.maritalStatus,
        department:params.department,
        position:params.position,
        dateHired:params.dateHired,
        employmentStatus:params.employmentStatus,
        contactNumber:params.contactNumber,
        email:params.email,
        address:params.address,
        city:params.city,
        province:params.province,
        nationality:params.nationality
    })

    return profile.save().then((profile , err) =>{
        return (err) ? false : true ;
    })
}

// to get all profile
module.exports.get = () =>{
    return Profile.find()
}

// to get specific profile by id
module.exports.viewDetails = (params) =>{
    return Profile.findById(params.id).then(data => data)
}

module.exports.delete = (params) =>{
    return Profile.findByIdAndDelete(params.id).then(data => data)
}

module.exports.edit= (params) =>{
    console.log(params)
    const id = params.id
    const updates ={
    firstName:params.updatedData.firstName,
    lastName: params.updatedData.lastName,
    birthdate: params.updatedData.birthdate,
    gender: params.updatedData.gender,
    maritalStatus: params.updatedData.maritalStatus,
    department: params.updatedData.department,
    position: params.updatedData.position,
    dateHired: params.updatedData.dateHired,
    employmentStatus: params.updatedData.employmentStatus,
    contactNumber:params.updatedData.contactNumber,
    email: params.updatedData.email,
    address:params.updatedData.address,
    city: params.updatedData.city,
    province: params.updatedData.province,
    nationality:params.updatedData.nationality
}
    return Profile.findByIdAndUpdate(id, updates).then((doc, err) =>{
        return (err)? false : true
    })
}