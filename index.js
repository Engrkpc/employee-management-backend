const express = require('express');
const app = express();
const mongoose = require('mongoose');
const cors =require('cors');

app.use(cors());
app.options("*", cors());

// const corsOptions ={
//     origin:'https://employee-management-ibjfioqev-engrkpc.vercel.app/',
//     optionsSuccessStatus:200
// }

// app.use(cors(corsOptions));

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas.'))

mongoose.connect('mongodb+srv://admin:admin@wdc028-course-booking.ux8nl.mongodb.net/employee_management?retryWrites=true&w=majority', {
    useNewUrlParser: true,
	useUnifiedTopology: true,
    useFindAndModify: false
})

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const profileRoutes =  require('./routes/profile');

app.use('/api/profile', profileRoutes);

// app.listen(4000, () =>{
//     console.log(`API is online on PORT  4000`)
// })


app.listen(process.env.PORT || 4000, () => {
    console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
})

